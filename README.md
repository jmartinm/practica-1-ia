# Practica 1 IA



## Día 1 (03/10/2023)

Durante el día de hoy he completado el ejercicio 1 (busqueda en profundidad) y el ejercicio 2 (búsqueda en anchura), asi como comentar el codigo para dejar explicado que hace cada cosa y/o por qué lo hace

## Día 2 (04/10/2023)

Durante el día de hoy he completado el ejercicio 3 (búsqueda de coste uniforme) y el ejercicio 4 (A*), asi como comentar el codigo para dejar explicado que hace cada cosa y/o por qué lo hace

## Día 3 (10/10/2023)

Durante el día de hoy he completado el ejercicio 5 y el ejercicio 6, este sin embargo solo pasa 2/3 test

## Día 4 (12/10/2023)

El dia de hoy consegui que el ejercicio 6 me pasara los 3 test y use la misma estructura para hacer el 7 sin embargo haciendo esto solo me pasa 2/4, debo mejorarlo

## Día 4 (16/10/2023)

Completados los 7 ejercicios de la practica y comentados