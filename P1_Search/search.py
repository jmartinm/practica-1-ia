# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem: SearchProblem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    """
    "*** YOUR CODE HERE ***"
    # Estado inicial
    startNode = problem.getStartState()
    # Pila LIFO (Last In First Out)
    frontier = util.Stack()
    # Tupla: (nodo, camino hasta el nodo)   
    frontier.push((startNode, []))
    # Registro nodos para evitar ciclos
    expanded = set()

    # Mientras haya nodos por explorar
    while not frontier.isEmpty():
        # Obtiene el nodo más reciente de la pila y su camino hasta ese nodo.
        node, path_to_node = frontier.pop()

        # Comprueba si el nodo actual es un estado objetivo. 
        if problem.isGoalState(node):
            # Devuelve el camino hasta ese nodo.
            return path_to_node

        # Comprueba si el nodo ya ha sido explorado.    
        if node not in expanded:  
            # Agrega nodo al conjunto
            expanded.add(node)
            # Itera sobre los sucesores
            for child, action, _ in problem.getSuccessors(node):
                # Agrega los sucesores a la pila junto con el camino hasta el sucesor
                # (estamos explorando los sucesores en profundidad antes de volver atrás y explorar otros nodos.)
                frontier.push((child, path_to_node + [action]))

    return "Failed"
    # util.raiseNotDefined()

def breadthFirstSearch(problem: SearchProblem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    # Estado inicial
    startNode = problem.getStartState()
    # Cola FIFO (First In First Out)
    frontier = util.Queue()
    # Tupla (estado, camino)
    frontier.push((startNode, []))
    # Registro nodos para evitar ciclos
    expanded = set()
    # Marcar el estado inicial como visitado
    expanded.add(startNode)

    # Mientras haya nodos por explorar
    while not frontier.isEmpty():
        # Obtiene el nodo más antiguo de la cola y su camino hasta ese nodo.
        node, path = frontier.pop()

        # Comprueba si el estado actual es el estado objetivo
        if problem.isGoalState(node):
            # Devuelve el camino hasta ese nodo.
            return path

        # Obtiene los sucesores válidos del nodo actual.
        successors = problem.getSuccessors(node)

        # Itera sobre los sucesores
        for successor, action, cost in successors:
            # Comprueba si el nodo ya ha sido explorado.
            if successor not in expanded:
                # Crea un nuevo camino agregando la acción tomada
                new_path = path + [action]
                # Agrega el sucesor a la cola
                frontier.push((successor, new_path))
                # Marcar el sucesor como visitado
                expanded.add(successor)

    return "Failed"
    # util.raiseNotDefined()

def uniformCostSearch(problem: SearchProblem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"
    # Estado inicial
    startNode = problem.getStartState()
    # Cola de prioridad (utiliza una función de costo para ordenar los nodos de manera que los nodos con menor costo tengan prioridad)
    frontier = util.PriorityQueue()
    # Agrega el estado inicial junto con una lista vacía de acciones y un costo inicial de 0 a la cola de prioridad
    frontier.push((startNode, [], 0), 0)
    # Marcar el estado inicial como visitado
    expanded = set()

    # Mientras haya nodos por explorar
    while not frontier.isEmpty():
        # Obtiene el nodo con la menor función de costo de la cola de prioridad, junto con las acciones y el costo acumulado hasta ese nodo.
        node, actions, cost = frontier.pop()

        # Comprueba si el estado actual es el estado objetivo
        if problem.isGoalState(node):
            return actions

        # Comprueba si el nodo ya ha sido explorado. 
        if node not in expanded:
            # Agrega nodo al conjunto
            expanded.add(node)
            # Itera sobre los sucesores
            for child, action, step_cost in problem.getSuccessors(node):
                # Crea un nuevo camino agregando la acción tomada
                new_actions = actions + [action]
                # Calcula el nuevo costo acumulado
                new_cost = cost + step_cost
                # Agrega el sucesor a la cola de prioridad junto con las nuevas acciones y el nuevo costo.
                frontier.push((child, new_actions, new_cost), new_cost)

    return "Failed"
    # util.raiseNotDefined()

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem: SearchProblem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    # Estado inicial
    start_node = problem.getStartState()
    # Cola de prioridad (utiliza una función de costo para ordenar los nodos de manera que los nodos con menor costo tengan prioridad)
    frontier = util.PriorityQueue()
    # Cola de prioridad (tupla que contiene el estado actual, el camino hasta ese estado y el costo acumulado hasta ese estado.)
    frontier.push((start_node, [], 0), 0)
    # Marcar el estado inicial como visitado
    expanded = set()
    
    # Mientras haya nodos por explorar
    while not frontier.isEmpty():
        current_state, path, current_cost = frontier.pop()
        
        # Comprueba si el estado actual es el estado objetivo
        if problem.isGoalState(current_state):
            # Devuelve el camino hasta ese nodo.
            return path 
        
         # Comprueba si el nodo ya ha sido explorado. 
        if current_state not in expanded:
            # Agrega nodo al conjunto
            expanded.add(current_state)
            # Itera sobre los sucesores
            for next_state, action, step_cost in problem.getSuccessors(current_state):
                # Calcula el nuevo costo acumulado
                new_cost = current_cost + step_cost
                # Crea un nuevo camino que es una extensión del camino anterior con la acción tomada para llegar al sucesor.
                new_path = path + [action]
                # Calcula la prioridad del nodo sucesor utilizando la suma del costo acumulado y la estimación heurística del costo restante hasta el objetivo.
                priority = new_cost + heuristic(next_state, problem)
                # Agrega el sucesor a la cola de prioridad junto con las nuevas acciones y el nuevo costo.
                frontier.push((next_state, new_path, new_cost), priority)
                
    return "Failure" 
    # util.raiseNotDefined()


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
